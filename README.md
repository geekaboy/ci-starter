# Codeigniter Starter Project
**Folder เก็บ ตัวอย่าง Database อยู่ที่ /assets/database/**  
**Folder เก็บ Plugin อยู่ที่ /assets/plugins/**  
**Folder เก็บ Custom JS อยู่ที่ /appjs/**  

**การติดตั้งเบื้องต้น**
**1. แก้ไฟล์ config.php และ database.php เป็นของตัวเอง**


========================================================================


**การเพิ่ม plugin และ custom JS เข้าหน้า View ให้ส่งผ่าน Controllers ไปหา View
โดยมีรูปแบบดังนี้**
```
$data['plugin] = array(        
    'assets/plugin/plugin_folder/*.css',    
    'assets/plugin/plugin_folder/*.js',
);

$data['appjs'] = array(        
     'appjs/project_folder/view_folder/*.js',
);
$this->load->view('theme/header', $data);

```
