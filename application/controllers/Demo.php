<?php
//------------[Controller File name : Demo.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Demo extends CI_Controller {

    public function __construct(){
        parent::__construct();

    }

    public function index()
    {
        $this->main_view();

    }
    public function main_view()
    {
        //@Plugin & Appjs
		$data['plugin'] = array();
		$data['appjs'] = array('appjs/demo/app.js');

		//@VIEW
		$this->load->view('theme/header', $data);
		$this->load->view('demo/main_view.php');
		$this->load->view('theme/footer');

    }
    
}//END CLASS